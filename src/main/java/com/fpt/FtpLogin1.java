package com.fpt;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;

public class FtpLogin1 {
	public static void main(String[] args) throws IOException {
		FTPClient ftpClient = new FTPClient();
		boolean result;
		try {
			// Connect to the localhost. Using default configuration
			ftpClient.connect("localhost");
			
			// Connect to the myServer. Using Using XML configuration file res/conf/ftpd-typical.xml...
			//ftpClient.connect("myServer");
			
			// login to ftp server
			result = ftpClient.login("admin", "admin");
			//result = ftpClient.login("anonymous", null);
			//result = ftpClient.login("admin", "21232F297A57A5A743894A0E4A801FC3");
			if (result == true) {
				System.out.println("Successfully logged in!");
			} else {
				System.out.println("Login Fail!");
				return;
			}
		} catch (FTPConnectionClosedException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ftpClient != null) {
					ftpClient.disconnect();
				}
			} catch (FTPConnectionClosedException e) {
				System.out.println(e);
			}
		}
		
	}
}
